import json

data = {}

def jsonFromFile(file):
    global data
    with open(file) as json_file:
        data = json.load(json_file)
    return data

def getAllData():
    global data
    return data
