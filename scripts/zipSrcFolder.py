import shutil
import os
tag = os.environ.get('CI_COMMIT_TAG')
fileName = os.environ['APP_NAME']

# If running the pipeline with Tag, append the tag name in file name
if tag == None:
    fileName =fileName +'_'+ os.environ['CI_COMMIT_SHA']
else:
    fileName =fileName +'_'+ tag

# If Running the fileName with Tag

shutil.make_archive(fileName, 'zip', 'src')
os.makedirs('build')
shutil.move(fileName+'.zip','build')
os.environ['FILE_NAME'] = fileName+'.zip'