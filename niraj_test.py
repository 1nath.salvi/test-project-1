class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:

    def __init__(self):
        self.head = None


def getNumber(binary):
    binary_data = list()
    current_node = binary
    while 1:
        binary_data.append(current_node.data)
        if current_node.next:
            current_node = current_node.next
        else:
            break
    return int("".join(str(x) for x in binary_data), 2)


if __name__ == '__main__':
    llist = LinkedList()
    llist.head = Node(0)
    second = Node(1)
    third = Node(1)
    llist.head.next = second
    second.next = third
    getNumber(llist.head)